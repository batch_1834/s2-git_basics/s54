let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

function print(){
	return collection;
}

function enqueue(element){

	collection.push(element);
    return collection;
}

function dequeue(element){

	collection.shift(element);
    return collection;
}
function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	return collection.length === 0;
}

module.exports = {
	//export created queue functions
	print,
	enqueue,
	dequeue, 
	front, 
	size,
	isEmpty
};
